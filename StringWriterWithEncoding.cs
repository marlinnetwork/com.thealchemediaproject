﻿using System.Text;
using System.IO;

namespace TheAlchemediaProject
{
    public class StringWriterWithEncoding : StringWriter
    {
        private Encoding encoding;

        public StringWriterWithEncoding(StringBuilder builder, Encoding encoding)
            : base(builder)
        {
            this.encoding = encoding;
        }

        public override Encoding Encoding
        {
            get { return this.encoding; }
        }
    }
}