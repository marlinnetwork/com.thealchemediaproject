﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace TheAlchemediaProject.Crypto
{
    /// <summary>
    /// Summary description for XORCrypt.
    /// </summary>
    public class XORCrypt
    {
        public XORCrypt()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private static string _hashkey = "0xkdFkd(*2432!@#$225675DfgsSfcvbDFGJ657#!!#%AAsdfFSAJVBretyjmjWERq  QWERQWfas";

        public static string Encrypt(string pValue, string pSalt)
        {
            string retvalue = "";
            for (int i = 0; i < pValue.Length; i++)
            {
                int cc = (int)System.Convert.ToChar(pValue.Substring(i, 1));
                int ee = (int)System.Convert.ToChar(pSalt.Substring(i % pSalt.Length, 1));
                retvalue += ((int)(cc ^ ee)).ToString("X") + ((i + 1 < pValue.Length) ? ":" : "");
            }
            return retvalue;
        }


        public static string Decrypt(string pValue, string pSalt)
        {
            string retvalue = "";
            string[] valarr = pValue.Split(':');
            for (int i = 0; i < valarr.Length; i++)
            {
                int ee = (int)System.Convert.ToChar(pSalt.Substring(i % pSalt.Length, 1));
                int cc = int.Parse(valarr[i], System.Globalization.NumberStyles.HexNumber);
                retvalue += (char)((int)(cc ^ ee));
            }
            return retvalue;
        }


        public static string Decrypt(string pValue)
        {
            return Decrypt(pValue, _hashkey);
        }

        public static string Encrypt(string pValue)
        {
            return Encrypt(pValue, _hashkey);
        }



    }
}
