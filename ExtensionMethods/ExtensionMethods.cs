﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Xml;
using System.Security.Cryptography;
using System.Reflection;
using System.Globalization;
using System.Text.RegularExpressions;
using System.ComponentModel;


namespace TheAlchemediaProject
{
    public static class ExtensionMethods
    {
        private static Random rand = new Random();

        public static bool TryStrToGuid(this string s, out Guid value)
        {
            try
            {
                value = new Guid(s);
                return true;
            }
            catch (FormatException)
            {
                value = Guid.Empty;
                return false;
            }
        }

        public static string RemoveNonDigits(this string value)
        {
            return Regex.Replace(value, @"[^0-9]+", string.Empty, RegexOptions.Compiled);
        }

        public static string RemoveSpecialCharacters(this string value)
        {
            return Regex.Replace(value, @"[^0-9a-zA-Z\-_]+", string.Empty, RegexOptions.Compiled);
        }

        public static string ReplaceSpecialCharacters(this string value, string replacement)
        {
            return Regex.Replace(value, @"[^0-9a-zA-Z\-_ ]+", replacement, RegexOptions.Compiled);
        }

        public static string ToMD5HashString(this string input)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytesToHash = System.Text.Encoding.ASCII.GetBytes(input);
            bytesToHash = md5.ComputeHash(bytesToHash);
            StringBuilder sb = new StringBuilder();

            foreach (byte b in bytesToHash)
            {
                sb.Append(b.ToString("x2", CultureInfo.InvariantCulture));
            }

            return sb.ToString();
        }

        public static bool ContainsAny(this string e1, string e2)
        {
            if (e2 != null)
            {
                return e1.ContainsAny(e2.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries));
            }

            return false;
        }

        public static bool ContainsAny(this string e1, IEnumerable<string> e2)
        {
            foreach (string s in e2)
            {
                if (e1.IndexOf(s, StringComparison.CurrentCultureIgnoreCase) >= 0) return true;
            }

            return false;
        }

        public static bool ContainsAny<T>(this IEnumerable<T> e1, IEnumerable<T> e2)
        {
            if (typeof(T) == typeof(string))
            {
                foreach (string item in (IEnumerable<string>)e2)
                {
                    if (((IEnumerable<string>)e1).Contains(item, StringComparer.CurrentCultureIgnoreCase)) return true;
                }
            }
            else
            {
                foreach (var item in e2)
                {
                    if (e1.Contains(item)) return true;
                }
            }

            return false;
        }

        public static IList<T> Randomize<T>(this IList<T> input)
        {
            if (input == null)
            {
                return input;
            }

            for (int i = input.Count - 1; i > 0; i--)
            {
                int n = rand.Next(i + 1);
                T temp = input[i];
                input[i] = input[n];
                input[n] = temp;
            }

            return input;
        }

        public static byte[] StripBOM(byte[] data)
        {
            if (data.Length > 2 && data[0] == 0xEF && data[1] == 0xBB && data[2] == 0xBF)
            {
                return data.Skip(3).ToArray();
            }
            return data;

        }

        public static string ToCSV<T>(this IList<T> list)
        {
            if (list == null || list.Count == 0)
            {
                return string.Empty;
            }

            // get type from 0th member
            Type t = list[0].GetType();
            string newLine = Environment.NewLine;
            StringBuilder sw = new StringBuilder();

            // make a new instance of the class name we figured out to get its props
            object o = Activator.CreateInstance(t);

            // gets all properties
            PropertyInfo[] props = o.GetType().GetProperties();

            // foreach of the properties in class above, write out properties
            // this is the header row
            for (int i = 0; i < props.Count(); i++)
            {
                string delimiter = i == props.Count() - 1 ? string.Empty : ",";
                String name = props[i].Name;

                foreach (var attr in props[i].GetCustomAttributes(true))
                {
                    if(attr.GetType() == typeof(DisplayNameAttribute))
                        name =  ((DisplayNameAttribute)attr).DisplayName;
                }

                sw.Append(name.ToUpper(CultureInfo.CurrentCulture) + delimiter);
            }

            sw.Append(newLine);

            // this acts as datarow
            foreach (T item in list)
            {
                // this acts as datacolumn
                for (int i = 0; i < props.Count(); i++)
                {
                    // this is the row+col intersection (the value)
                    string whatToWrite =
                        Convert.ToString(
                                        item.GetType()
                                             .GetProperty(props[i].Name)
                                             .GetValue(item, null), 
                                             CultureInfo.CurrentCulture);

                    string delimiter = i == props.Count() - 1 ? string.Empty : ",";

                    whatToWrite = whatToWrite.Replace("\"", "\"\"");

                    whatToWrite = "\"" + whatToWrite + "\"";

                    sw.Append(whatToWrite + delimiter);
                }

                sw.Append(newLine);
            }

            byte[] encodedBytes = Encoding.UTF8.GetBytes(sw.ToString());

            return (Encoding.UTF8.GetString(StripBOM(encodedBytes)));
        }
    }
}
