﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TheAlchemediaProject
{
        /// <summary>
        /// Lazy-loads an item that might require heavy return 
        /// data from a query.
        /// The Generic constraint "where T : class, new()" means
        /// that the T must be of reference type with a 
        /// parameterless ctor.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public class LazyItem<T> where T : class, new()
        {
            private T _inner = null;
            private IQueryable<T> _query = null;

            // TODO: consider if this needs to be thread-safe. I think maybe it should be.
            // How to make this thread safe? Locking models?
            // check http://www.albahari.com/threading/part2.aspx#_Thread_Safety
            // and http://www.yoda.arachsys.com/csharp/singleton.html
            private Func<T> _innerGetter = null;

            public LazyItem()
            {
                this._inner = new T();
            }

            public LazyItem(T inner)
            {
                this._inner = inner;
            }

            public LazyItem(IQueryable<T> query)
            {
                if (query == null)
                {
                    throw new ArgumentNullException();
                }

                this._query = query;
            }

            public LazyItem(Func<T> innerGetter)
            {
                this._innerGetter = innerGetter;
            }

            public T Inner
            {
                get
                {
                    if (this._inner == null)
                    {
                        if (this._query != null)
                        {
                            this._inner = this._query.SingleOrDefault<T>();     // TODO: do i really want the 'OrDefault' here?
                        }

                        if (this._innerGetter != null)
                        {
                            this._inner = this._innerGetter.Invoke();
                        }
                    }

                    return this._inner;                                     // TODO: do i want Inner returning nulls etc?
                    // I think so: where the LazyItem property of
                    // a model object represents a NULLABLE or 
                    // non-required field. The null return value
                    // will indicate te absence of the value.
                }
            }
        }
    }
