﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace TheAlchemediaProject
{
    /// <summary>
    /// An IList implementation that flexes IQueryable's delayed loading
    /// </summary>
    /// <typeparam name="T">IList of T</typeparam>
    public class LazyList<T> : IList<T> 
    {
        private IQueryable<T> query;
        private IList<T> inner;

        public LazyList()
        {
            this.inner = new List<T>();
        }

        public LazyList(IQueryable<T> query) 
        {
            this.query = query;
        }

        public int Count
        {
            get { return this.Inner.Count; }
        }

        public IList<T> Inner
        {
            get
            {
                if (this.inner == null)
                {
                    this.inner = this.query.ToList();
                }

                return this.inner;
            }
        }

        public bool IsReadOnly
        {
            get { return this.Inner.IsReadOnly; }
        }

        public T this[int index]
        {
            get { return this.Inner[index]; }
            set { this.Inner[index] = value; }
        }

        public int IndexOf(T item) 
        {
            return this.Inner.IndexOf(item);
        }

        public void Insert(int index, T item) 
        {
            this.Inner.Insert(index, item);
        }

        public void RemoveAt(int index) 
        {
            this.Inner.RemoveAt(index);
        }

        public void Add(T item) 
        {
            this.inner = this.inner ?? new List<T>();
            this.Inner.Add(item);
        }

        public void Clear() 
        {
            if (this.inner != null)
            {
                this.Inner.Clear();
            }
        }

        public bool Contains(T item) 
        {
            return this.Inner.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex) 
        {
            this.Inner.CopyTo(array, arrayIndex);
        }

        public bool Remove(T item) 
        {
            return this.Inner.Remove(item);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator() 
        {
            return this.Inner.GetEnumerator();
        }

        public IEnumerator GetEnumerator() 
        {
            return ((IEnumerable)this.Inner).GetEnumerator();
        }
    }
}
